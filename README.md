# Index of w3-related group

- [OWL primer](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/owl-primer/)
- [RDF](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/rdf/)
- [SKOS](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/skos/)
- [SPARQL](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/sparql/)
---
- [Contents](https://suredata.gitlab.io/dcat-ap-knpp/w3c-related/contents/)

